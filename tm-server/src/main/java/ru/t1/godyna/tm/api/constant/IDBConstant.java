package ru.t1.godyna.tm.api.constant;

import org.jetbrains.annotations.NotNull;

public interface IDBConstant {

    @NotNull
    String USER_TABLE = "tm_user";

    @NotNull
    String SESSION_TABLE = "tm_session";

    @NotNull
    String PROJECT_TABLE = "tm_project";

    @NotNull
    String TASK_TABLE = "tm_task";

    @NotNull
    String ROW_ID = "row_id";

    @NotNull
    String USER_ID = "user_id";

    @NotNull
    String CREATED = "created";

    @NotNull
    String STATUS = "status";

    @NotNull
    String NAME = "name";

    @NotNull
    String LOGIN = "login";

    @NotNull
    String PASSWORD_HASH = "password_hash";

    @NotNull
    String EMAIL = "email";

    @NotNull
    String FST_NAME = "fst_name";

    @NotNull
    String LST_NAME = "lst_name";

    @NotNull
    String MDL_NAME = "mdl_name";

    @NotNull
    String ROLE = "role";

    @NotNull
    String LOCK_FLG = "lock_flg";

    @NotNull
    String PROJECT_NAME = "name";

    @NotNull
    String PROJECT_DESCRIPTION = "descrptn";

    @NotNull
    String PROJECT_CREATED = "created";

    @NotNull
    String PROJECT_STATUS = "status";

    @NotNull
    String TASK_NAME = "name";

    @NotNull
    String TASK_DESCRIPTION = "descrptn";

    @NotNull
    String TASK_CREATED = "created";

    @NotNull
    String TASK_STATUS = "status";

    @NotNull
    String TASK_PROJECT_ID = "project_id";

}
