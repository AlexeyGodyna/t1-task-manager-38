package ru.t1.godyna.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IDatabaseProperty {

    @NotNull
    String getDatabaseURL();

    @NotNull
    String getDatabaseUsername();

    @NotNull
    String getDatabasePassword();

}
