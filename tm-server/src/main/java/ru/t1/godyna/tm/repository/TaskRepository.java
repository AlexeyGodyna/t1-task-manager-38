package ru.t1.godyna.tm.repository;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.api.constant.IDBConstant;
import ru.t1.godyna.tm.api.repository.ITaskRepository;
import ru.t1.godyna.tm.enumerated.Status;
import ru.t1.godyna.tm.model.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Getter
    @NotNull
    private final String tableName = IDBConstant.TASK_TABLE;

    public TaskRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Override
    public @NotNull Task fetch(@NotNull ResultSet row) throws Exception {
        @NotNull final Task task = new Task();
        task.setId(row.getString(IDBConstant.ROW_ID));
        task.setName(row.getString(IDBConstant.TASK_NAME));
        task.setDescription(row.getString(IDBConstant.TASK_DESCRIPTION));
        task.setUserId(row.getString(IDBConstant.USER_ID));
        task.setStatus(Status.toStatus(row.getString(IDBConstant.TASK_STATUS)));
        task.setCreated(row.getTimestamp(IDBConstant.TASK_CREATED));
        task.setProjectId(row.getString(IDBConstant.TASK_PROJECT_ID));
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task add(@NotNull final Task task) {
        @NotNull final String query = String.format(
                "INSERT INTO %s (%s, %s, %s, %s, %s, %s, %s) VALUES (?, ?, ?, ?, ?, ?, ?);",
                getTableName(),
                IDBConstant.ROW_ID,
                IDBConstant.TASK_CREATED,
                IDBConstant.TASK_NAME,
                IDBConstant.TASK_DESCRIPTION,
                IDBConstant.TASK_STATUS,
                IDBConstant.USER_ID,
                IDBConstant.TASK_PROJECT_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, task.getId());
            statement.setTimestamp(2, new Timestamp(task.getCreated().getTime()));
            statement.setString(3, task.getName());
            statement.setString(4, task.getDescription());
            statement.setString(5, Status.NOT_STARTED.name());
            statement.setString(6, task.getUserId());
            statement.setString(7, task.getProjectId());
            statement.executeUpdate();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task add(@Nullable final String userId, @Nullable final Task task) {
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String taskId) {
        @NotNull final List<Task> result = new ArrayList<>();
        @NotNull final String query = String.format(
                "SELECT * FROM %s WHERE %s = ? AND %s = ?;",
                getTableName(),
                IDBConstant.USER_ID,
                IDBConstant.TASK_PROJECT_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, userId);
            statement.setString(2, taskId);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            while (rowSet.next()) result.add(fetch(rowSet));
        }
        return result;
    }

    @Override
    @SneakyThrows
    public Task update(@NotNull final Task task) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ?, %s = ? WHERE %s = ?;",
                getTableName(),
                IDBConstant.PROJECT_NAME, IDBConstant.PROJECT_DESCRIPTION,
                IDBConstant.PROJECT_STATUS, IDBConstant.TASK_PROJECT_ID, IDBConstant.ROW_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, task.getName());
            statement.setString(2, task.getDescription());
            statement.setString(3, task.getStatus().toString());
            statement.setString(4, task.getProjectId());
            statement.setString(5, task.getId());
            statement.executeUpdate();
        }
        return task;
    }

}
