package ru.t1.godyna.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.api.repository.IUserOwnedRepository;
import ru.t1.godyna.tm.api.service.IConnectionService;
import ru.t1.godyna.tm.api.service.IUserOwnerService;
import ru.t1.godyna.tm.exception.entity.EntityNotFoundException;
import ru.t1.godyna.tm.exception.field.IdEmptyException;
import ru.t1.godyna.tm.exception.field.IndexIncorrectException;
import ru.t1.godyna.tm.exception.field.UserIdEmptyException;
import ru.t1.godyna.tm.model.AbstractUserOwnedModel;

import java.sql.Connection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnerService<M> {

    public AbstractUserOwnedService(
            @NotNull final IConnectionService connectionService
    ) {
        super(connectionService);
    }

    @NotNull
    protected abstract IUserOwnedRepository<M> getRepository(@NotNull final Connection connection);

    @Override
    @SneakyThrows
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            repository.removeAll(userId);
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Connection connection = getConnection();
        boolean existFlg = false;
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            existFlg = repository.existsById(userId, id);
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return existFlg;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final Connection connection = getConnection();
        @Nullable final List<M> entity;
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            entity = repository.findAll(userId);
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return entity;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final String userId, @Nullable final Comparator<M> comparator) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final Connection connection = getConnection();
        @Nullable final List<M> entity;
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            if (comparator == null) return repository.findAll(userId);
            entity = repository.findAll(userId, comparator);
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return entity;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            @Nullable final M model = repository.findOneById(userId, id);
            if (model == null) throw new EntityNotFoundException();
            return model;
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            @Nullable final M model = repository.findOneByIndex(userId, index);
            if (model == null) throw new EntityNotFoundException();
            return model;
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Connection connection = getConnection();
        @Nullable final M entity;
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            entity = repository.removeOneById(userId, id);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return entity;
    }

    @NotNull
    @Override
    @SneakyThrows
    public M removeOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexOutOfBoundsException();
        @NotNull final Connection connection = getConnection();
        @Nullable final M entity;
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            entity = repository.removeOneByIndex(userId, index);
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return entity;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M add(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) return null;
        @NotNull final Connection connection = getConnection();
        @Nullable final M entity;
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            entity = repository.add(userId, model);
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return entity;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeOne(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) return null;
        @NotNull final Connection connection = getConnection();
        @Nullable final M entity;
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            entity = repository.removeOne(userId, model);
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return entity;
    }

    @Override
    @SneakyThrows
    public int getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final Connection connection = getConnection();
        int size = 0;
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            size = repository.getSize(userId);
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return size;
    }

}
